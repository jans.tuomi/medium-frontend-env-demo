# Frontend application with injected environment variables

## How to use

    docker build -t demo .
    docker run -e FRONTEND_EXAMPLE_ENV=foobar -p 8080:80 demo

Visit http://localhost:8080 to see the result.

## Author

Jan Tuomi, <jans.tuomi@gmail.com>. 2019.
